"""
/***************************************************************************
 ENMTunerDialog
                                 A QGIS plugin
 this tunes enms
                              -------------------
        begin                : 2016-02-11
        git sha              : $Format:%H$
        copyright            : (C) 2016 by peter galante
        email                : Pgalante@amnh.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import os
from PyQt4 import QtGui, uic, QtCore
from PyQt4.QtCore import QFile, QFileInfo
from qgis import core, gui, utils
from qgis.core import QGis, QgsMessageLog, QgsRasterLayer
from qgis.gui import QgsMapCanvasLayer
from qgis.core import QgsMapLayerRegistry
from qgis.utils import iface
import gdal
import sys
import subprocess

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'ENM_dialog_test.ui'))

class ENMTunerDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(ENMTunerDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        self.pBGo.clicked.connect(self.pBGo_clicked)
        self.pBRun.clicked.connect(self.pBRun_clicked)
        self.pBOut.clicked.connect(self.pBOut_clicked)
        self.pBOcc.clicked.connect(self.pBOcc_clicked)
        self.pBEnv.clicked.connect(self.pBEnv_clicked)
        self.pButt_Method.clicked.connect(self.pButt_Method_clicked)
        self.pButt_BG.clicked.connect(self.pButt_BG_clicked)
        self.pButt_FC.clicked.connect(self.pButt_FC_clicked)
        self.pButt_ModSel.clicked.connect(self.pButt_ModSel_clicked)
        self.pButt_Project.clicked.connect(self.pButt_Project_clicked)
        self.pButt_Help.clicked.connect(self.pButt_Help_clicked)
        self.pButt_R.clicked.connect(self.pButt_R_clicked)
        self.pButtCat.clicked.connect(self.pButtCat_clicked)
        self.pButt_AllHelp.clicked.connect(self.pButt_AllHelp_clicked)
        self.rButtEnv.clicked.connect(self.rButtEnv_clicked)
        self.linEdR.textChanged.connect(self.evaluateForm)

    def evaluateForm(self):
    	if self.linEdDir.isModified() and self.linEdOcc.isModified() and self.linEdEnv.isModified() and self.linEdBy.isModified() and (self.rButtAIC.isChecked() or self.rButtSeq.isChecked()) and (self.rButtBlock.isChecked() or self.rButtJackknife.isChecked() or self.radioButton.isChecked() or self.radioButton_2.isChecked()) and (self.rBLin.isChecked() or self.rBHinge.isChecked() or self.rBProd.isChecked() or self.rBQuad.isChecked() or self.rBThresh.isChecked()):
    		self.pBGo.setEnabled(True)

    def rButtEnv_clicked(self):
        if self.rButtEnv.isChecked()==True:
            env = self.linEdEnv.text()
            env1 = env.replace("\\", "/")        
            GetData = "names(locs)<-c('x', 'y')\n" + "bio<-getData('worldclim', var='bio', download = T, res = 0.5, lon = c(extent(locs)[1], extent(locs)[2]), lat =c(extent(locs)[3], extent(locs)[4]), path = '" + env1 + "')\n"+ "rasts<-stack(list.files(pattern = '.bil', full.names = T, path = paste('" + env1 + "', 'wc0.5', sep='/')))\n" + "writeRaster(rasts, filename = paste('" + env1 + "',names(rasts), sep = '/'), format = 'GTiff', bylayer=T)\n"
        else: 
            env = self.linEdEnv.text()
            env1 = env.replace("\\", "/")        
            GetData = "names(locs)<-c('x', 'y')\n" +"bio<-stack(list.files(path= '"+env1+"'"+", full.names=T, pattern ='\\\.tif$'))\n"
        return GetData

    def pButt_AllHelp_clicked(self):
    	QtGui.QMessageBox.information(self, "Help", "Occurrence data should be a .csv file with two columns where column A is 'Longitude',"+
    		"and column B is 'Latitude'.\n\n"+"Selecting to run in parallel will utilize all of your computer's cores.\n\n"+"After making all of your"+
    		"selections, click to Generate R script, which will save an R file that you can run seperately to replicate your results.\n\n"+
    		"Then, click to Run ENMeval, which will tune your model, select the optimal, and plot in logistic format.")

    def pButtCat_clicked(self):
        QtGui.QMessageBox.information(self, "Categorical Variables", "List any categorical variable file names\n"+"in quotes, separated by commas, excluding the file extension.\n"+'For example: "Soils", "Landcover"')

    def pButt_R_clicked(self):
        self.linEdR.setText(QtGui.QFileDialog.getExistingDirectory(None, "Select folder containing Rscript.exe", "/home", QtGui.QFileDialog.ShowDirsOnly))

    def pButt_Help_clicked(self):
        QtGui.QMessageBox.information(self, "Background Selection Area", "For Buffered Points and MCP, use\n"+"a buffer measured in meters (ie\n"+"100000m for 100 km). The \n"+"Bounding Box buffers localities using Degrees.")

    def pButt_Project_clicked(self):
        self.linEdProject.setText(QtGui.QFileDialog.getExistingDirectory(None, 'Select a folder:', '/home', QtGui.QFileDialog.ShowDirsOnly))

    def pButt_Method_clicked(self):
        QtGui.QMessageBox.information(self, "Method Choice", "Pearson, 2007\n"+"Shcheglovitova & Anderson, 2013\n"+"")

    def pButt_BG_clicked(self):
        QtGui.QMessageBox.information(self, "Background Selection", "Anderson, 2013\n"+"Anderson & Martinez-Meyer, 2004\n"+"Anderson & Raza, 2010\n"+"Barve et al., 2011\n"+"Merow, 2013\n"+"Peterson et al., 2011\n")

    def pButt_FC_clicked(self):
        QtGui.QMessageBox.information(self, "Feature Classes to use", "Merow, 2013\n"+"Phillips & Dudik, 2008\n"+"Phillips, 2008")

    def pButt_ModSel_clicked(self):
        QtGui.QMessageBox.information(self, "Model Selection Options", "Lobo et al., 2008\n"+"Peterson et al., 2011\n"+"Warren & Seifert, 2011")

    def Backg_radio(self):
        if self.rButtBoundBox.isChecked() == True:
            backg = "buff.dist<-"+self.linEdBuff.text()+"\n"+"e<-extent(locs)\n"+"e1<-extent(e[1]-buff.dist,e[2]+buff.dist, e[3]-buff.dist, e[4]+buff.dist)\n"+"env<-crop(bio, e1)\n"+"backg<-randomPoints(bio,10000)\n"
        if self.rButtBuffPoints.isChecked() == True:
            backg = "buff.dist<-"+self.linEdBuff.text()+"\n"+"loc.rast<-rasterize(locs, bio)\n"+"buff.locs<- buffer(loc.rast, buff.dist)\n"+"env<-mask(bio, buff.locs)\n"+"backg<-randomPoints(bio, 10000)\n"
        if self.rButtMCP.isChecked() == True:
            backg = "buff.dist<-"+self.linEdBuff.text()+"\n"+"mcp <- function (xy) {\n"+"xy <- as.data.frame(coordinates(xy))\n"+"coords.t <- chull(xy[, 1], xy[, 2])\n"+"xy.bord <- xy[coords.t, ]\n"+"xy.bord <- rbind(xy.bord[nrow(xy.bord), ], xy.bord)\n"+"return(SpatialPolygons(list(Polygons(list(Polygon(as.matrix(xy.bord))), 1))))\n"+"}\n"+"MCP.locs<-mcp(locs)\n"+"shp <- gBuffer(MCP.locs, width = buff.dist)\n"+"env<-mask(bio, shp)\n"+"backg<-randomPoints(bio, 10000)\n"
        return backg

    def pBOut_clicked(self):
        self.linEdDir.setText(QtGui.QFileDialog.getExistingDirectory(None, 'Select a folder:', '/home', QtGui.QFileDialog.ShowDirsOnly))

    def pBOcc_clicked(self):
        self.linEdOcc.setText(QtGui.QFileDialog.getOpenFileName())

    def pBEnv_clicked(self):
        self.linEdEnv.setText(QtGui.QFileDialog.getExistingDirectory(None, 'Select a folder:', '/home', QtGui.QFileDialog.ShowDirsOnly))
        
    def radio_btns_meth(self):
        if self.rButtBlock.isChecked() == True:
            Meth = "'block'"
        if self.rButtJackknife.isChecked() == True:
            Meth = "'jackknife'"
        if self.radioButton.isChecked() == True:
            Meth = "'checkerboard1'"
        if self.radioButton_2.isChecked() == True:
            Meth = "'checkerboard2'"
        if self.rButtAIC.isChecked() == True:
            Meth = "'block'"
        return Meth

    def parallel_btn(self):
    	if self.rBParallel.isChecked() == True:
    		Para = ",parallel = T"
    	else:
    		Para = ""
    	return Para

    def radio_btns_FC(self):
    #### Each FC individually
        if self.rBLin.isChecked() == True:
            FC = "'L'"
        if self.rBQuad.isChecked() == True:
            FC = "'LQ'"
        if self.rBHinge.isChecked() == True:
            FC = "'H'"
        if self.rBProd.isChecked() == True:
            FC = "'P'"
        if self.rBThresh.isChecked() == True:
            FC = "'T'"
    ####Combinations of all FCs
        if self.rBLin.isChecked() == True & self.rBQuad.isChecked() == True & self.rBHinge.isChecked() == True:
            FC = "c('L','LQ','H','LQH')"
        if self.rBLin.isChecked() == True & self.rBHinge.isChecked() == True & self.rBQuad.isChecked() == True & self.rBProd.isChecked() == True:
            FC = "c('L','LQ','H','LQH','P','LQHP')"
        if self.rBLin.isChecked() == True & self.rBQuad.isChecked() == True & self.rBHinge.isChecked() == True & self.rBProd.isChecked() == True & self.rBThresh.isChecked() == True:
            FC = "c('L','LQ','H','LQH','P','LT','LPT','LQHPT')"
        return FC

    def modSel(self):
    	outDir = self.linEdDir.text()
        outDir1 = outDir.replace("\\", "/")
        if self.rButtAIC.isChecked() == True:
            Mod_sel = "no.zero.param <- mod.table[mod.table$nparam != 0,]\n"+"ordered<-no.zero.param[with(no.zero.param, order(delta.AICc)), ]\n"+"opt.mod<-ordered[1,]\n"+"write.csv(opt.mod,'"+outDir1+"/optmodAICc.csv')\n"+"best.num<-as.numeric(rownames(opt.mod))\n"+"best.pred<-res@predictions[[best.num]]\n"
        if self.rButtSeq.isChecked() == True:
            Mod_sel = "optimize <- function(res) {\n"+"opt.auc <- res[res$full.AUC] >= 0.5,]\n"+ "no.param <- opt.auc[opt.auc$nparam] != 0,]\n"+"noAICNA<- no.param[which(!is.na(no.param$AICc)),]\n"+"noOR0 <- noAICNA[noAICNA$Mean.OR10 != 0,]\n"+"ordered<-noOR0[with(noOR0, order(Mean.OR10, -Mean.AUC)), ]\n"+"ordered[1,]\n"+"}\n"+"opt.mod<-optimize(mod.table)\n"+"print(opt.mod)\n"+"best.num<-as.numeric(rownames(opt.mod))\n"+"best.pred<-res@predictions[[best.num]]\n"+"write.csv(opt.mod, '"+outDir1+"/optModSeq.csv')\n"
        return Mod_sel

    def pBGo_clicked(self):
        outDir = self.linEdDir.text()
        outDir1 = outDir.replace("\\", "/")
        #DD = self.linEdDir.text()        
        QQ = outDir1+"/OUTPUT.R"
        out = "".join(str(QQ))
        self.text_file = open(out, 'w')
        env = self.linEdEnv.text()
        env1 = env.replace("\\", "/")
        occ = self.linEdOcc.text()
        occ1 = occ.replace("\\", "/")
        Proj = self.linEdProject.text()
        Proj1 = Proj.replace("\\","/")
        if self.linEdCat.text() == "":
            categoricals = 'NULL'
        else:
            categoricals = "c("+self.linEdCat.text()+")"
        self.text_file.write('pkg.list<-c("ENMeval", "rgeos", "rgdal", "rJava")\n' + 'new.pkgs<-pkg.list[!(pkg.list %in% installed.packages()[,"Package"])]\n' + 'if(length(new.pkgs)) install.packages(new.pkgs)\n'
            "library(ENMeval)\n"+"library(rgeos)\n"+"library(rgdal)\n"+
            "locs = read.csv('" + occ1 + "')\n"+ self.rButtEnv_clicked()+
            "Projection<-stack(list.files(path= '"+Proj1+"', full.names=T, pattern ='\\\.tif$'))\n"+
            self.Backg_radio()+"\n"+
            "res = ENMevaluate(" + 'RMvalues = seq(' + self.linEdFrom.text()+','+self.linEdTo.text()+
            ','+self.linEdBy.text()+'),'+'occ = locs,'+ 'env = env,'+'bg.coords = backg,'+'fc ='+
            self.radio_btns_FC()+ ','+ 'method ='+ self.radio_btns_meth()+''+',categoricals ='+ categoricals +self.parallel_btn()+')\n'+
            'mod.table<-res@results\n'+ self.modSel()+ "b.m<-opt.mod$rm\n"+ "beta.mulr<- paste('betamultiplier=',b.m,sep='')\n"+
            "false.args<-c('noautofeature','noproduct','nothreshold','noquadratic','nohinge','nolinear')\n"+ 
            "feat<-strsplit(as.vector(opt.mod[,2]), ',')[[1]]\n"+ "if (feat == 'L'){feats = 'linear'} else if (feat == 'LQ'){feats = c('quadratic', 'linear')} else if (feat == 'H'){feats = 'hinge'} else if (feat == 'P'){feats = 'product'} else if (feat == 'T'){feats = 'threshold'} else if (feat == 'LQH'){feats = c('linear', 'quadratic', 'hinge')} else if (feat == 'LQHP'){feats = c('linear', 'quadratic', 'hinge', 'product')} else if (feat == 'LQHPT'){feats = c('linear', 'quadratic', 'hinge', 'product', 'threshold')}\n"+
            "for (j in 1:length(feats)){false.args[which(sub('no','',false.args)==feats[j])] = feats[j]}\n"+ "m <-maxent(env, locs, args=c(false.args, beta.mulr, 'noremoveduplicates', 'noaddsamplestobackground'))\n"+
            "pred  <- predict(object= m, x=Projection, na.rm=TRUE, format='GTiff',overwrite=TRUE, progress='text',args='logistic')\n"+
            "writeRaster(pred,'"+ outDir1+"/OptModel.tif')\n")
        self.text_file.close()

    def pBRun_clicked(self):
        out = self.linEdDir.text()
        out1 = out.replace("\\", "/")
        R_loc = self.linEdR.text()+"/"
        R_loc1 = R_loc.replace("\\", "/")
        RScriptLocation = R_loc1 + '/Rscript'
        OutputLocation = out1 + '/OUTPUT.R'
        QtGui.QMessageBox.information(self, "Evaluating", "Performing evaluation. Close window to continue")
        subprocess.call([RScriptLocation, OutputLocation])
        #subprocess.call(["/usr/bin/Rscript", "/home/pgalante/Desktop/Garbage/OUTPUT.R"])
        QtGui.QMessageBox.information(self, "Evaluating", "Done!!")

        fileName = out1+"/OptModel.tif"
        fileInfo = QFileInfo(fileName)
        baseName = fileInfo.baseName()
        mylayer = QgsRasterLayer(fileName, baseName)
        QgsMapLayerRegistry.instance().addMapLayer(mylayer, True)
        #root.insertLayer(0, mylayer)